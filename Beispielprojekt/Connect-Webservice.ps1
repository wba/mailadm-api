function Connect-WebService # Verbinden mit dem WebServices
{
    # Setzen der Parameter für die Funktionen
    [CmdletBinding()]
    param($global:primarydomain="URL der primäre Maildomäne, z.B. itc.rwth-aachen.de")

    # Get-Credentials
    $username = "srv123456@rc.rwth-ad.de"
    $CredentialPath = "C:\Pfad\Credentials"
    $PwdSecureString = Get-Content "$CredentialPath\$Username.cred" | ConvertTo-SecureString
    $Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $Username, $PwdSecureString
    
    # Mit API verbinden
    $uri = "https://ws.rc.rwth-ad.de/ex-cache-ws/APIv04.asmx?WSDL"
    
    
    # Hier wird eine Do-Schleife verwendet, weil das verbinden mit dem ws.rwth-ad.de manchmal nicht klappt
    $counter = 0
    do
    {
        $counter++
        write-verbose "Versuch $counter"
        $error.clear()
        $global:WS = New-WebServiceProxy -Uri $uri -Credential $cred
        if($error.length -ne 0)
        {
            sleep 2
        }
    }
    until($error.length -le "0" -or $counter -eq 5)

    if($WS -eq $null -or $counter -eq 5)
    {
        write-host "Es konnte sich nicht mit dem Mailsystem verbunden werden, Skript wird nun abgebrochen;
        Inhalt der Error-Variable:
        $error" -foreground Red
        break
    }
    else
    {
        write-verbose "Verbindungsversuch zum Mailsystem erfolgreich"
    }
}
