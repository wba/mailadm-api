function Check-Invitations
{
    # Setzen der Parameter für die Funktionen
    [CmdletBinding()]
    param
    (
        # Pfad zur Excel-Datei
        $xlsxpath
    )

    # Importieren der Excel-Datei mit den Mailboxen
    $File = Import-Excel -Path $xlsxpath
    # Alt: $file = Import-CSV -Path "C:\Abschlussprojekt\Files\mai.rwth-aachen.de.csv" -Delimiter ";"

    # Importieren aller Mailboxen aus dem Mailsystem
    $AllMbxsInMailsystem = $WS.GetMailboxList() | Select UPN,EMailaddress,GivenName,Sn,@{Name="QuotaInMB"; Expression = {$_.QuotaInB/1048576}},@{Name="TotalItemSizeInMB"; Expression = {$_.TotalItemsizeInB/1048576}},Mailboxtype
    
    # Importieren aller Invitations aus dem Mailsystem
    $AllInvitations = $WS.GetInvitations()

    foreach($MbxinFile in $File)
    {
        if($MbxInFile.UPN -eq $null)
        {
            # Sucht nach dem jeweiligen Postfach im Mailsystem
            $MbxInMailSystem = $AllMbxsInMailsystem | Where {$_.emailaddress -eq $MbxInFile.emailaddress}
        
            # Wenn das Postfach noch nicht existiert
            if($MbxInMailSystem -eq $null)
            {
                # In allen vorhandenen Einladungen wird geprüft, ob das Postfach schon eingeladen wurde
                $MbxInInvitations = $AllInvitations | Where {$_.EMailaddress -eq $MbxInFile.Emailaddress}
                # Wenn kein Ergebnis zurückkommt, wird eine Einladung erstellt
                if($MbxInInvitations -eq $null)
                {
                    write-verbose "Das Postfach $($MbxInFile.Emailaddress) existiert noch nicht in der Domäne und wird nun eingeladen"
                    $quotaPools = $WS.GetQuotaPoolsForDomain($primaryDomain)
                    $QuotaPoolID = $quotapools[0].ID
                    $PrimaryEMailAddress = $MbxinFile.EMailaddress
                    $GivenName = $MbxinFile.GivenName
                    $Surname   = $MbxinFile.Sn
                    $InitialQuotaINMB = $MbxinFile.InitialQuotaInMB
                    $Mailboxtype = 0

                    # QuotaPoolID,primaryEMailAddress,givenname,surname,quotaINMB,aliasadresses,Mailboxtype,deliverToMailboxAndForward,forwardingSmtpAddress,resourceOwner
                    $invitationResult = $WS.CreateMailaccountInvitation($quotaPools[0].ID, $PrimaryEMailAddress, $Surname, $GivenName, $InitialQuotaINMB, @(), $mailboxtype, $false, $null, $null)
                }
                # Wenn ein Ergebnis zurückkommt, kommt eine Statusmeldung der Einladung
                else
                {
                    write-verbose "Das Postfach $($MbxInFile.emailaddress) wurde am $(Get-Date $MbxInInvitations.Whencreated -Format "dd.MM.yyyy HH:mm") Uhr in die Domäne eingeladen, aber die Einladung wurde noch nicht angenommen (ActivationCode: $($MbxInInvitations.ActivationCode))"
                    #$MbxInFile.UPN = "TESTCODE@mai.rwth-aachen.de"
                }
            }
            # Wenn das Postfach schon existiert, aber noch kein UPN in der Datei eingetragen ist. (Die Einladung wurde angenommen und die Datei wird nun bearbeitet)
            else
            {
                write-verbose "Die Einladung für das Postfach $($MbxInFile.emailaddress) wurde nun angenommen. Excel-Datei wird nun mit dem UPN ergänzt"
                
                # Bearbeiten des Eintrags in der Variable zum späteren Exportieren
                $MbxInFile.UPN = $MbxInMailSystem.UPN
                # Setzen des Tags zum Überschreiben der Datei
                $overwrite = 1
            }
        }
        # Wenn der UPN schon gesetzt ist
        else
        {
            write-verbose "Der UPN des Accounts $($MbxInFile.Emailaddress) ist schon gesetzt und der Account existiert bereits in der Domäne."
        }
    }
    
    # Es wird geprüft, ob das Excelfile abgeändert wurde
    if($overwrite -eq 1)
    {
        # Das abgeänderte Excelfile wird nun abgespeichert 
        # (da überschreiben nicht geht, wird das alte erst gelöscht und danach das neue erstellt)
        write-verbose "Die Excel-Datei wird nun überschrieben mit den neuen Einträgen"
        rm $xlsxpath
        $File | Export-Excel -Path $xlsxpath -AutoFilter -AutoSize
    }
    else
    {
        write-verbose "Es mussten keine Änderungen am Excel-File vorgenommen werden. Excel File wird nicht überschrieben"
    }
} #Ende der Funktion Check-Invitations