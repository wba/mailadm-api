function Check-Mailinglists
{
    [CmdletBinding()]
    param
    (
        # Pfad zur Excel-Datei
        $xlsxpath
    )    
    
    # Importieren der Excel-Datei mit den Mailboxen
    $File = Import-Excel -Path $xlsxpath

    # Importieren aller Postfächer aus dem Mailsystem
    $AllMbxsInMailsystem = $WS.GetMailboxlist()

    # Importieren aller Mailinglisten aus dem Mailsystem
    $AllMailingLists = $WS.getMailLists()
    write-verbose "Es gibt folgende Mailinglists:"
    
    # Aufzählen aller Mailinglists untereinander (bei Write-Verbose "$AllMailingLists.Emailaddress" wäre die Anzeige nebeneinander, was nicht gut aussieht)
    foreach($Mailinglist in $AllMailinglists)
    {
        write-verbose "$($Mailinglist.Emailaddress)"
    }

    # ForEach-Schleife für die Überprüfung
    foreach($MailingList in $AllMailinglists)
    {
        # Ausgabe für Verbose
        write-verbose "`n --Die Mailingliste $($Mailinglist.emailaddress) wird nun überprüft"

        # Holen der gewünschten Subscriber der Mailingliste aus der Excel-Datei
        $SupposedMembers = $File | Where {$_.UPN -ne $null -and $_.Mailinglists -like "*$($Mailinglist.EMailaddress)*"}

        # Holen der aktuellen Mitglieder der Mailingliste
        $CurrentMembers = $WS.GetUsersInMaillist($Mailinglist.EMailAddress)

        foreach($SupposedMember in $SupposedMembers)
        {
            # Wenn der gewünschte Member in der Mailingliste enthalten ist, ist alles ok.
            if($Currentmembers.Emailaddress -contains $SupposedMember.Emailaddress)
            {
                write-verbose "$($SupposedMember.Emailaddress) ist schon in der Mailingliste $($Mailinglist.Emailaddress) und auch ein gewünschter Member. Alles korrekt"
            }
            # Wenn der gewünschte Member noch KEIN Mitglied der Mailingliste ist, wird er hinzugefügt
            elseif($CurrentMembers.Emailaddress -notcontains $SupposedMember.Emailaddress)
            {
                write-host "$($SupposedMember.Emailaddress) ist ein gewünschter Member der Mailingliste $($Mailinglist.Emailaddress), ist aber noch kein Mitglied. Wird nun hinzugefügt" -foreground yellow
                $WS.AddMailboxToMaillist($Mailinglist.Emailaddress,$SupposedMember.UPN)
                #$WebserviceConnection.addMailboxToMaillist($distributionlist, $mailbox.upn)
            }
        }

        # Testen in die andere Richtung nach nicht gewünschten Einträgen
        foreach($CurrentMember in $CurrentMembers)
        {
            # Wenn der aktuelle Member NICHT in den gewünschten Membern vorhanden ist, wird er entfernt
            if($SupposedMembers.Emailaddress -notcontains $CurrentMember.Emailaddress)
            {
                write-host "$($CurrentMember.Emailaddress) ist schon in der Mailingliste $($Mailinglist.Emailaddress), ist aber KEIN gewünschter Member. Wird nun entfernt" -foreground yellow
                $ws.RemoveMailboxFromMaillist($Mailinglist.Emailaddress,$Currentmember.UPN)
            }
        }
    }
} #Ende der Funktion Check-Mailinglists