function Initialize
{
    [CmdletBinding()]
    param()

    #Excel-Modul
    write-verbose "Es werden die benötigten Module geladen und ggf. Installiert.."
    $GetModule = Get-Module ImportExcel -ListAvailable -ErrorAction SilentlyContinue
    if($GetModule -eq $null)
    {
        write-verbose "Modul existiert noch nicht und wird nun installiert"
        Install-Module ImportExcel -Confirm:$false
        write-verbose "Modul wurde installiert und wird nun importiert"
        Import-Module ImportExcel

    }
    else
    {
        write-verbose "Modul existiert schon und wird nun importiert"
        Import-Module ImportExcel
    }
    write-verbose "Module geladen!"
    
    # Testen, ob das Excel-File existiert
    if($(Test-Path -Path $xlsxpath) -eq $true)
    {
        write-verbose "Excel-Datei existiert bei >$xlsxpath< und wird im laufe des Skriptes benutzt"
    }
    else
    {
        write-host "Excel-Datei konnte bei >$xlsxpath< nicht gefunden werden. Bitte verschieben Sie das Excel-File oder ändern Sie den Dateipfad"
        break;
    }
}