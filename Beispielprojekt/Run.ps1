function Run
{
    # Setzen der Parameter für die Funktionen
    [CmdletBinding()]
    param
    (
        # Pfad zur Excel-Datei (Inputdatei)
        $xlsxpath = "C:\Verwaltungspfad\Files\Pfad zur Excel-Datei",
        $Logpath = "C:\Verwaltungspfad\Files\Logs\$(Get-Date -format "yyyy_MM_dd__HH_mm")_Log.log"
    )

    #Starten des Loggings
    Start-Transcript -Path $LogPath

    # Importieren der anderen Dateien
    . .\Connect-Webservice.ps1
    . .\Initialize.ps1
    . .\Check-Invitations.ps1
    . .\Check-Quota.ps1
    . .\Check-Mailinglists.ps1
    
    
    # Diese funktion führt automatisch alle Schritte automatisch durch in folgender Reihenfolge:       

    # Laden aller Module (Excel)
    write-host "Die Funktion Load-Modules wird ausgeführt" -Foreground darkyellow
    Initialize
    write-host "Die Funktion Load-Modules ist durchgelaufen `n" -Foreground darkyellow


    # Verbinden mit dem Webservice der RWTH Aachen
    write-host "Die Funktion Connect-Webservice wird ausgeführt" -Foreground darkyellow
    Connect-WebService
    write-host "Die Funktion Connect-Webservice ist durchgelaufen `n" -Foreground darkyellow


    # Überprüfung, ob neue Accounts existieren und ggf. Einladung erstellen
    write-host "Die Funktion Check-Invitations wird ausgeführt" -Foreground darkyellow
    Check-Invitations -xlsxpath $xlsxpath
    write-host "Die Funktion Check-Invitations ist durchgelaufen `n" -Foreground darkyellow


    # Überprüfung, ob das Quota der jeweiligen Accounts noch stimmt und anschließende Anpassung
    write-host "Die Funktion Check-Quota wird ausgeführt" -Foreground darkyellow
    Check-Quota -xlsxpath $xlsxpath
    write-host "Die Funktion Check-Quota ist durchgelaufen `n" -Foreground darkyellow


    # Überprüfung, ob alle Mailinglisten korrekt gesetzt sind und anschließende Anpassung
    write-host "Die Funktion Check-Mailinglists wird ausgeführt" -Foreground darkyellow
    Check-Mailinglists -xlsxpath $xlsxpath
    write-host "Die Funktion Check-Mailinglists ist durchgelaufen `n" -Foreground darkyellow

    # Stoppen des Loggings
    Stop-Transcript
}