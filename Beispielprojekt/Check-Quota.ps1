function Check-Quota
{
    [CmdletBinding()]
    param
    (
        # Pfad zur Excel-Datei
        $xlsxpath,
        # Threshold in %, ab wann das Postfach erhöht werden soll
        $Threshold = 7,
        # Wert in Prozentwert, wie viel das QUota immer erhöht werden soll
        $Quotamultiplicator = 1.25
    )

    # Die Funktion überprüft und vergleicht das Quota jeder Mailbox im Mailsystem mit dem gewünschten Quota aus
    # der Excel-Datei und erhöht bei überschreiten des Schwellwertes das Quota der Mailbox. Den Schwellwert kann man im Skript anpassen
    # und ist standardmäßig auf 75% gesetzt. Wenn der Schwellwert (Threshold) überschritten wird, wird standardmäßig um 25% erhöht.
    # Bevor diese Funktion ausgeführt werden kann, muss erst Connect-Webservice ausgeführt werden.

    # Importieren der Excel-Datei mit den Mailboxen
    $File = Import-Excel -Path $xlsxpath

    # Filtern der EInträge in der Excel-Datei, dass nur Einträge mit UPN genommen werden
    $file = $file | Where {$_.UPN -ne $null}

    # Importieren aller Mailboxen aus dem Mailsystem und Formatieren der Spalten
    $AllmbxsInMailsystem = $WS.GetMailboxList() | Select UPN,EMailaddress,GivenName,Sn,@{Name="QuotaInMB"; Expression = {$_.QuotaInB/1048576}},@{Name="TotalItemSizeInMB"; Expression = {$_.TotalItemsizeInB/1048576}},Mailboxtype

    # Geht jede Zeile in der Excel-Datei durch und vergleicht dann den Eintrag mit dem Ist-Zustand in dem Mailsystem
    foreach($MbxinFile in $File)
    {
        # Bekommen des jeweiligen Postfaches im Mailsystem
        $MbxInMailSystem = $AllmbxsInMailsystem | Where {$_.Emailaddress -eq $MbxInFile.EMailaddress}

        # Wenn ein Eintrag gefunden wird
        if($MbxInMailsystem -ne $null)
        {
            # Bekommen des aktuellen Quotas
            $CurrentQuota = $MbxInMailSystem.QuotaInMB
            $CurrentUsage = $MbxInMailSystem.TotalItemSizeInMB #Muss gefragt werden, ob das die aktuelle Belegung des Postfaches ist
            $Filledpercent = 100/$CurrentQuota*$CurrentUsage

            # Auflisten aller wichtigen Eigenschaften des Postfachs
            write-verbose "Das Postfach $($MbxInMailsystem.UPN) hat aktuell folgende Eigenschaften
            Current Quota: $CurrentQuota MB
            Current Usage: $CurrentUsage MB
            FilledPercent = $FilledPercent %"

            # Wenn der belegte Speicher über dem threshold liegt
            if($filledPercent -gt $threshold)
            {
                # Neues Quota wird errechnet und aufgerundet auf ganze MB
                $NewQuota = $CurrentQuota * $Quotamultiplicator
                $newQuota = [Math]::ceiling($newQuota)

                # Wenn das Aktuelle Quota schon am festgelegten Maximum ist, wird nicht erhöht
                if($CurrentQuota -eq $MbxinFile.MaxQuotaInMB)
                {
                    # write-verbose info
                    write-host "Schwellwert überschritten! Die Mailbox würde nun ein erhöhtes Quota bekommen, hat aber bereits das Maximalquota ($($MbxinFile.MaxQuotaInMB)MB) erreicht. Es wird keine weitere Erhöhung vorgenommen. `n" -foreground yellow
                }
                # Wenn das Neue Quota nach Erhöhung größer als das festgelegte Maximum ist, wird nur bis zum festgelegten Maximum erhöht
                elseif($newQuota -ge $MbxinFile.MaxQuotaInMB)
                {
                    # write-verbose info
                    write-host "Schwellwert überschritten! Das Postfach wurde vergrößert und hat nun das Maximalquota von $($MbxinFile.MaxQuotaInMB)MB erreicht. Es wird nicht mehr weiter erhöht `n" -foreground yellow
                
                    # Setzen des neuen Quotas
                    $newQuota = $mbxInFile.MaxQuotaInMB
                    $WS.SetQuota($($MbxinMailsystem.upn),$newQuota)
                }
                # Wenn das Neue Quota nach Erhöhung kleiner als das festgelegte Maximum ist, wird das Quota wie gewünscht erhöht
                elseif($newQuota -lt $MbxinFile.MaxQuotaInMB)
                {
                    # write-verbose info
                    write-host "Das Postfach hat das Margin für eine Quotaerhöhung erreicht. Das Postfach bekommt nun 25% mehr Quota (bis Maximal $($MbxinFile.MaxQuotaInMB)MB)
                    Altes Quota für $($MbxInMailsystem.UPN): $CurrentQuota MB
                    Neues Quota für $($MbxInMailsystem.UPN): $newQuota MB" -foreground yellow
                
                    # Setzen des neuen Quotas
                    $WS.SetQuota($($MbxinMailsystem.upn),$newQuota)
                }
                else
                {
                    write-host "Irgendwas geht nicht: Skriptbedingungen prüfen, Administrator benachrichtigen `n"
                }
            
                # Entfernen der Variable newquota, um fehler auszuschließen
                Remove-Variable newquota
            }
            # Wenn der Threshold für eine Quotaerhöhung noch nicht erreicht ist
            else
            {
                # write-verbose info
                write-verbose "Das Quota der Mailbox sieht gut aus. Quotaerhöhung wird nicht durchgeführt. `n"
            }
        
        }
        else
        {
            write-verbose "Die E-Mail-Adresse >$($MbxInFile.EMailaddress)< ist wahrscheinlich falsch konfiguriert, wurde sie vielleicht abgeändert?"
        }
    } #Ende foreach-Schleife
} #Ende Function Check-Quota