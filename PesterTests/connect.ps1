# functioncall in order to get an active connection to the MailAdm API

function new-mailadmconnection {
    [CmdletBinding()]
    param (
        [String]$user,
        [string]$password,
        [string]$uri
    )
    $secPW = ConvertTo-SecureString -String $password -AsPlainText -Force
    $cred = New-Object System.Management.Automation.PSCredential($user, $secPW)
    $WS = New-WebServiceProxy -Uri $uri -Credential $cred
    $WS
}


function Get-Domains {
    [cmdletbinding()]
    param ($webserviceconnection)

    $domains = $webserviceconnection.GetDomains()
    $domains
}