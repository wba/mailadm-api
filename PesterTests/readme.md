# Pester Tests für die MailAdm API


### Voraussetzungen

Zur Nutzung der MailAdm API benötigen Sie einen Serviceaccount. Diesen können Sie anfordern, indem sie eine Mail an servicedesk@itc.rwth-aachen.de schicken. Derzeit ist unsere Schnittstelle mit der Windows Powershell 5.1 getestet.

## Projektinhalt

Das Projekt besteht aus dem Unterverzeichnis Beispielprojekt, das beispielhaft die in der API bereitgestellten Funktionen zeigt. Hierbei beinhalten die einzelnen Datien folgende Themenblöcke:
1. connect.ps1
- Verbindungsaufbau zur API
2. listquota.ps1
- Funktionsaufrufe zur Verwaltung des Quotapool
3. mailaccounts.ps1
- Funktionsaufrufe zur verwaltung von Mailadressen
4. distributionlist.ps1
- Funktionsaufrufe zur Verwaltung von Verteilerlisten

## Ausführung

Im Verzeichnis PesterTests finden Sie entsprechende Unittests, um die Schnittstelle mit Ihrem Serviceaccount zu testen. Bitte passen Sie hierzu in der Datei MailAdm-Doku.Tests.ps1 den Bereich "connection settings" an.
Um die Tests zu starten, wechseln Sie in der Windows Powershell ins "Beispielprojekt" Verzeichnis und starten Sie das Cmdlet invoke-pester

## Autoren

* **Thomas Pätzold**
* **Jannis Hahn**
* **Mia Möbes**