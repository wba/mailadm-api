#functioncalls in oder to administer mailaddresses

function get-mailaccounts {
    [CmdletBinding()]
    param ($webserviceconnection)
    
    $mailaccounts = $webserviceconnection.getMailboxList()
    $mailaccounts
}

function get-mailbox {
    [CmdletBinding()]
    param ($webserviceconnection,$upn)

    $mailbox = $webserviceconnection.getMailbox($upn)
    $mailbox
}

function add-mailboxinvitation {
    [CmdletBinding()]
    param ($webserviceconnection, $quotaPoolID, $primaryEMailAddress, $givenname, $Surname, $QuotaInMB, $Aliasaddresses, $DistributionGroupAddresses, $Mailboxtype, $DeliverToMailBoxAndforward, $ForwardingEmailaddress, $Owner)

    $invitation = $webserviceconnection.CreateMailaccountInvitation($quotaPoolID, $primaryEMailAddress, $givenname, $Surname, $QuotaInMB, $Aliasaddresses, $DistributionGroupAddresses, $Mailboxtype, $DeliverToMailBoxAndforward, $ForwardingEmailaddress, $Owner)
    $invitation
}

function get-mailboxinvitations {
    [CmdletBinding()]
    param ($webserviceconnection)
    $Invitations = $webserviceconnection.GetInvitations()
    $Invitations
}

function remove-allmailboxinvitation {
    [CmdletBinding()]
    param ($webserviceconnection)
    
    $invitations = $webserviceconnection.getinvitations()
    foreach ($invitation in $invitations) {
        $webserviceconnection.deleteinvitation($invitation.activationcode)
    }
}

function remove-mailboxinvitation {
    [CmdletBinding()]
    param ($webserviceconnection, $activationcode)
    
    $result = $webserviceconnection.deleteinvitation($activationcode)
    $result
}

function disable-mailaccount {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox)
    
    $result = $webserviceconnection.DisableMailbox($mailbox[0].UPN)
    $result
}

function reconnect-mailbox {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox)
    
    $result = $webserviceconnection.ReconnectMailbox($mailbox[0].UPN)
    $result
}

<#
# Es gibt in Version 05_1 keine Moeglichkeit mehr, Weiterleitungsadressen zu konfigurieren.
function change-mailboxforwardingaddress {
    [cmdletbinding()]
    param($webserviceconnection, $mailbox, $newforwardingmailaddress)
    $mailboxes = $webserviceconnection.getMailboxList()
    $mailboxtochange = $mailboxes | ? { $_.emailaddress -like $mailbox }
    $result = $webserviceconnection.setMailForwarding($mailboxtochange.upn, $true, $newforwardingmailaddress)
    $result
}
#>


function change-mailboxquota {    
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox, $newquota)

    $mailboxes = $webserviceconnection.getMailboxList()
    $mailboxtochange = $mailboxes | ? { $_.emailaddress -like $mailbox }

    $result = $ws.SetQuota($mailboxtochange.upn, $newquota)
    $result
}

function change-mailboxquotapool {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox, $newquotapool)

    $result = $webserviceconnection.setquotapool($mailbox, $newquotapool)
    $result
}

function change-addfullaccesspermission {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox, $user)
    $mailboxes = $webserviceconnection.getMailboxList()
    $mailboxtochange = $mailboxes | ? { $_.emailaddress -like $mailbox }

    $result = $webserviceconnection.AddMailboxPermission($mailboxtochange.upn, $user)
    $result
}

function change-removefullaccesspermission {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox, $user)
    $mailboxes = $webserviceconnection.getMailboxList()
    $mailboxtochange = $mailboxes | ? { $_.emailaddress -like $mailbox }

    $result = $webserviceconnection.RemoveMailboxPermission($mailboxtochange.upn, $user)
    $result
}

function change-mailboxdisplayname {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox, $newdisplayname)

    $mailboxes = $webserviceconnection.getMailboxList()
    $mailboxtochange = $mailboxes | ? { $_.emailaddress -like $mailbox }

    $result = $webserviceconnection.SetMailboxDisplayName($mailboxtochange.upn, $newdisplayname)
    $result
}

function change-sendaspermission {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox, $User)
    $mailboxes = $webserviceconnection.getMailboxList()
    $mailboxtochange = $mailboxes | ? { $_.emailaddress -like $mailbox }

    $result = $webserviceconnection.AddSendAsPermission($mailboxtochange.upn, $user)
    $result
}

function change-emailaddresses {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox, $primaryEmailaddress, $secondaryemailaddress)
    $mailboxes = $webserviceconnection.getMailboxList()
    $mailboxtochange = $mailboxes | ? { $_.emailaddress -like $mailbox }

    $result = $webserviceconnection.SetEmailAddresses($mailboxtochange.upn, $primaryEmailaddress, $secondaryemailaddress)
    $result
}

function change-removesendaspermission {
    [CmdletBinding()]
    param ($webserviceconnection, $mailbox, $User)
    $mailboxes = $webserviceconnection.getMailboxList()
    $mailboxtochange = $mailboxes | ? { $_.emailaddress -like $mailbox }

    $result = $webserviceconnection.RemoveSendAsPermission($mailboxtochange.upn, $user)
    $result
}

function change-mailboxtype {
    [cmdletBinding()]
    param ($webserviceconnection, $upn, $newmailboxtype)

    $result = $webserviceconnection.changemailboxtype($upn,$newmailboxtype)
    $result
}