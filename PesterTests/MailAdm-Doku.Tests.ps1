﻿# These are the Pester Tests in order to demonstrate how the API works and 
# reflects the following versions API05_1 MailADM 3.0.4
# If you need further documentation please check our doku-portal [0]
# or contact our IT-ServiceDesk (servicedesk@itc.rwth-aachen.de)
# [0] https://help.itc.rwth-aachen.de/service/1jefzdccuvuch/article/31c9d912142143d0a9740fa5e55f9701

# initializing
# connection settings
$user = "<Service Account>" # e.g. srv123456@rwth-ad.de
$password = "<Password>"
$uri = "https://ws.rwth-ad.de/ex-cache-ws/APIv05_1.asmx?wsDL"

# set variables
# please change the following variables
$existingUPN = "<existing user logon name>" # e.g. ab123456@mustereinrichtung.rwth-aachen.de

# these variables should work
$Primarydomain = "mustereinrichtung.rwth-aachen.de"
$SecondaryDomain = ""
$existingPersonalMailaccount = "paetzold@mustereinrichtung.rwth-aachen.de"
$existingfunctionalMailbox = "rudirentier@mustereinrichtung.rwth-aachen.de"
$externalmailaddress = "hugo@mueller.de"
$localpartpersonalmailmox = "hugodertierpfleger"
$distributionlist = "zoo@mustereinrichtung.rwth-aachen.de"
$seconddistributionlist = "tiergehege@mustereinrichtung.rwth-aachen.de"
$newdistributionmailaddress = "alletiere2@mustereinrichtung.rwth-aachen.de"
$newmailaddress = "zebra@mustereinrichtung.rwth-aachen.de"

# import functions
. .\connect.ps1
. .\listquota.ps1
. .\mailaccounts.ps1
. .\distributionlist.ps1



Describe "MailAdm-Doku" {
    It "Connection: Establish a connection with Webservice" {
        $global:ws = new-mailadmconnection -user $user -password $password -uri $uri
        $url = $uri.replace("?wsDL", "")
        $global:ws.Url | Should -Be $url
    }

    IT "get Domains for Serviceaccount: min. one Domain is configured for serviceaccount" {
        $domains = Get-Domains -webserviceconnection $global:ws
        $domains.count | should -BeGreaterOrEqual 1
    }

    It "get quotapools: min. one configured quotapool and quotapool should be greater then 1MB" {
        $quotapools = get-quotapool -Domain $Primarydomain -WebserviceConnection $global:ws
        $quotapools.count | should -BeGreaterOrEqual 1
        foreach ($quotapool in $quotapools) {
            $quotapool.TotalQuotaInMB | should -BeGreaterThan 1
        }
    }

    It "get-mailaccounts: check for mailaccounts in primary or secondary domain" {
        $mailaccounts = get-mailaccounts -WebserviceConnection $global:ws
        $mailaccounts.count | should -BeGreaterOrEqual 0
    }

    It "get-mailbox: check for existing Logon Credentials and returns the corresponding Mailboxinformation" {
        $mailaccount = get-mailbox -webserviceConnection $global:ws -upn $existingUPN
        $mailaccount.count | should -Be 1
    }

    It "Remove-Mailboxinvitation: dummy test in order to remove all invitations to get a defined infrastructure" {
        $result = remove-allmailboxinvitation -webserviceconnection $global:ws
        # check if all invitations could be deleted
        $invitations = get-mailboxinvitations -webserviceconnection $global:ws
        $invitations.count | should -Be 0
    }

    It "Add-Mailboxinvitation: personal mailbox" {
        $surname = "der Tierpfleger"
        $givenname = "Hugo"
        $primaryMailaddress = "$localpartpersonalmailmox@$Primarydomain"
        $MailaddressAliasList = @()
        $DistributionGroupAddresses = @()
        $quota = 10
        # Mailboxtype:
        # 0 : personal mailbox
        # 1 : functional mailbox
        # 2 : room mailbox
        # 3 : equipment mailbox
        $Mailboxtype = 0
        $Owner = ""
        $DeliverMailboxAndForward = $false
        $ForwardingEmailaddress = ""
        # get possible quotapool (first in List)
        $quotapool = get-quotapool -Domain mustereinrichtung.rwth-aachen.de -WebServiceConnection $global:ws                                                           
        [int32]$quotapoolid = $quotapool[0].id

        $invitation = add-mailboxinvitation -webserviceconnection $global:ws -quotaPoolID $quotapoolid -primaryEMailAddress $primaryMailaddress -givenname $givenname -surname $Surname -QuotaInMB $quota -Aliasaddresses $MailaddressAliasList -DistributionGroupAddresses $DistributionGroupAddresses -Mailboxtype $Mailboxtype -DeliverToMailBoxAndforward $DeliverMailboxAndForward -ForwardingEmailaddress $ForwardingEmailaddress -Owner $Owner
        $invitation.returncode | should -Be 0
    }

    It "Add-Mailboxinvitation: functional mailbox" {
        $surname = "Zooleitung"
        $givenname = ""
        $primaryMailaddress = "zooleitung@$Primarydomain"
        $MailaddressAliasList = @()
        $DistributionGroupAddresses = @()
        $quota = 10
        # Mailboxtype:
        # 0 : personal mailbox
        # 1 : functional mailbox
        # 2 : room mailbox
        # 3 : equipment mailbox
        $Mailboxtype = 1
        $owner = ""
        $DeliverMailboxAndForward = $false
        $forwardingEmailaddress = ""
        # get possible quotapool (first in List)
        $quotapool = get-quotapool -Domain mustereinrichtung.rwth-aachen.de -WebServiceConnection $global:ws                                                           
        $quotapoolid = $quotapool[0].id

        $invitation = add-mailboxinvitation -webserviceconnection $global:ws -quotaPoolID $quotapoolid -primaryEMailAddress $primaryMailaddress -givenname $givenname -Surname $surname -QuotaInMB $quota -Aliasaddresses $MailaddressAliasList -DistributionGroupAddresses $DistributionGroupAddresses -Mailboxtype $Mailboxtype -DeliverToMailBoxAndforward $DeliverMailboxAndForward -ForwardingEmailaddress $ForwardingEmailaddress -Owner $Owner
        $invitation.returncode | should -Be 0
    }

    It "Add-Mailboxinvitation: room mailbox" {
        $surname = "Eisbärgehege"
        $givenname = ""
        $primaryMailaddress = "eisbaergehege@$Primarydomain"
        $MailaddressAliasList = @()
        $DistributionGroupAddresses = @()
        $quota = 10
        # Mailboxtype:
        # 0 : personal mailbox
        # 1 : functional mailbox
        # 2 : room mailbox
        # 3 : equipment mailbox
        $Mailboxtype = 2
        $owner = $existingPersonalMailaccount
        $DeliverMailboxAndForward = $false
        $forwardingEmailaddress = ""
        # get possible quotapool (first in List)
        $quotapool = get-quotapool -Domain mustereinrichtung.rwth-aachen.de -WebServiceConnection $global:ws                                                           
        $quotapoolid = $quotapool[0].id

        $invitation = add-mailboxinvitation -webserviceconnection $global:ws -quotaPoolID $quotapoolid -primaryEMailAddress $primaryMailaddress -givenname $givenname -Surname $surname -QuotaInMB $quota -Aliasaddresses $MailaddressAliasList -DistributionGroupAddresses $DistributionGroupAddresses -Mailboxtype $Mailboxtype -DeliverToMailBoxAndforward $DeliverMailboxAndForward -ForwardingEmailaddress $ForwardingEmailaddress -Owner $Owner
        $invitation.returncode | should -Be 0
    }

    It "Add-Mailboxinvitation: equipment mailbox" {
        $surname = "Knuts Spielzeug"
        $givenname = ""
        $primaryMailaddress = "knutsspielzeug@$Primarydomain"
        $MailaddressAliasList = @()
        $DistributionGroupAddresses = @()
        $quota = 10
        # Mailboxtype:
        # 0 : personal mailbox
        # 1 : functional mailbox
        # 2 : room mailbox
        # 3 : equipment mailbox
        $Mailboxtype = 3
        $owner = $existingPersonalMailaccount
        $DeliverMailboxAndForward = $false
        $forwardingEmailaddress = ""
        # get possible quotapool (first in List)
        $quotapool = get-quotapool -Domain mustereinrichtung.rwth-aachen.de -WebServiceConnection $global:ws                                                           
        $quotapoolid = $quotapool[0].id

        $invitation = add-mailboxinvitation -webserviceconnection $global:ws -quotaPoolID $quotapoolid -primaryEMailAddress $primaryMailaddress -givenname $givenname -Surname $surname -QuotaInMB $quota -Aliasaddresses $MailaddressAliasList -DistributionGroupAddresses $DistributionGroupAddresses -Mailboxtype $Mailboxtype -DeliverToMailBoxAndforward $DeliverMailboxAndForward -ForwardingEmailaddress $ForwardingEmailaddress -Owner $Owner
        $invitation.returncode | should -Be 0
    }

    It "Get-MailboxInvitations: all invitations are created successfully" {
        $invitations = get-mailboxinvitations -WebserviceConnection $global:ws
        $invitations.count | should -BeGreaterOrEqual 4
    }

    It "RemoveMailboxInvitation" {
        $invitations = get-mailboxinvitations -WebserviceConnection $global:ws
        $invitations.count | should -BeGreaterOrEqual 4
        foreach ($invitation in $invitations) {
            $result = remove-mailboxinvitation -webserviceconnection $global:ws -Activationcode $invitation.Activationcode
            $result | should -BeTrue
        }
    }

    <#  It "disable Mailbox"{
        # make sure the mailbox is initialized -> send-mail
        # and wait for a minute in order to make sure that the mail is delivered
        Send-MailMessage -To $existingPersonalMailaccount  -From $existingfunctionalMailbox -Subject "Testmail in order to initialize mailbox" -SmtpServer smarthost.rwth-aachen.de
        Start-Sleep -Seconds 60
        $mailboxes = get-mailaccounts -webserviceconnection $global:ws
        foreach ($mailbox in $mailboxes){
            if ($mailbox.Emailaddress -like $existingPersonalMailaccount) {
                $personalaccountupn = $mailbox.UPN
            }
        }
        write-host "$personalaccountupn"
        $result = disable-mailaccount -WebserviceConnection $global:ws -mailbox $personalaccountupn
        $result | should -Be 0
    }

    It "Reconnect Mailbox"{
        # make sure that the serversystem get to know that the account is deleted
        Start-Sleep -Seconds 60

        $result = reconnect-mailbox -webserviceconnection $global:ws -mailbox $personalaccountupn
        $result | should -Be 0

    }
#>

<#

    # Es gibt in Version 05_1 keine Moeglichkeit mehr, Weiterleitungsadressen zu konfigurieren.
    It "change: forwardingaddress" {
        $result = change-mailboxforwardingaddress -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -newforwardingmailaddress $externalmaladdress
        $result | should -Be $true
    }
#>

    It "change: emailaddresses" {
        $result = change-emailaddresses -webserviceconnection $global:ws -mailbox $existingPersonalMailaccount -primaryEmailaddress $existingPersonalMailaccount -secondaryemailaddress $newmailaddress
        $result.Returncode | should -be 0

    }

    It "change: quota" {
        $result = change-mailboxquota -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -newquota 200
        $result | should -Be 0
    }

    It "change: quotaPool" {
        # get mailboxinformation

        $mailboxes = $global:ws.getMailboxList()
        $mailboxtochange = $mailboxes | Where-Object { $_.emailaddress -like $existingfunctionalMailbox }
        $quotapoolIds = get-quotapool -Domain $Primarydomain -WebserviceConnection $global:ws
        $quotapoolid = ($quotapoolIds | Where-Object { $_.id -ne $($mailboxtochange.quotapool.id) } | Select-Object -first 1).id

        $result = change-mailboxquotapool -webserviceconnection $global:ws -mailbox $($mailboxtochange.upn) -newquotapool $quotapoolid
        $result | should -Be $true
    }

    It "change: add fullaccess permission" {
        $result = change-addfullaccesspermission -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -user $existingPersonalMailaccount
        $result | should -Be $true
    }

    It "change: remove fullaccess permission" {
        $result = change-removefullaccesspermission -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -user $existingPersonalMailaccount
        $result | should -Be $true
    }

    It "change: resourcemailbox displayname" {
        $result = change-mailboxdisplayname -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -newdisplayname "Alle Tiere des Zoos"
        $result | should -Be $true
    }

    It "change: add SendAS permission" {
        $result = change-sendaspermission -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -user $existingPersonalMailaccount
        $result | should -Be $true
    }

    It "change: remove SendAs permission" {
        $result = change-sendaspermission -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -user $existingPersonalMailaccount
        $result | should -Be $true
    }

    It "change: Mailboxtype functional mailbox -> room mailbox" {
        # 0	UserMailbox
        # 1	Funktionsaccount (in Exchange: UserMailbox)
        # 2	RoomMailbox
        # 3	EquipmentMailbox
        # Der Vollzugriff ist eine notwendige Voraussetzung für das Wandeln in eine Ressourcen-Mailbox
        change-addfullaccesspermission -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -user $existingPersonalMailaccount
        $upn = "fkt_$existingfunctionalmailbox"
        $result = change-mailboxtype -webserviceConnection $global:ws -upn $upn -newmailboxtype 2
        $result | should -Be $true

    }
    It "change: Mailboxtype room mailbox -> equipment mailbox" {
        # 0	UserMailbox
        # 1	Funktionsaccount (in Exchange: UserMailbox)
        # 2	RoomMailbox
        # 3	EquipmentMailbox
        $upn = "fkt_$existingfunctionalmailbox"
        $result = change-mailboxtype -webserviceConnection $global:ws -upn $upn -newmailboxtype 3
        $result | should -Be $true

    }

    It "change: Mailboxtype equipment mailbox -> functional Mailbox" {
        # 0	UserMailbox
        # 1	Funktionsaccount (in Exchange: UserMailbox)
        # 2	RoomMailbox
        # 3	EquipmentMailbox
        $upn = "fkt_$existingfunctionalmailbox"
        $result = change-mailboxtype -webserviceConnection $global:ws -upn $upn -newmailboxtype 1
        $result | should -Be $true

    }

    It "change: Mailboxtype functional mailbox -> personal Mailbox -->> Error" {
        # 0	UserMailbox
        # 1	Funktionsaccount (in Exchange: UserMailbox)
        # 2	RoomMailbox
        # 3	EquipmentMailbox
        $upn = "fkt_$existingfunctionalmailbox"
        $result = change-mailboxtype -webserviceConnection $global:ws -upn $upn -newmailboxtype 0
        # Der Vollzugriff wird für den nächsten Skriptdurchlauf entfernt
        change-removefullaccesspermission -webserviceconnection $global:ws -mailbox $existingfunctionalMailbox -user $existingPersonalMailaccount
        $result | should -Be $false
    }

    It "create: distribution list" {
        $displayname = "Zoo"
        $dl = create-distributionlist -WebserviceConnection $global:ws -displayname $displayname -mailaddress $distributionlist
        $dl | should -Be 0
    }

    It "create: second distribution list" {
        $secondlistdisplayname = "Tiergehege"
        $dl = create-distributionlist -WebserviceConnection $global:ws -displayname $secondlistdisplayname -mailaddress $seconddistributionlist
        $dl | should -Be 0
    }

    It "Get distribution list" {
        $result = get-distributionlist -WebserviceConnection $global:ws -mailaddress $distributionlist
        $result.count | should -Be 1
    }

    It "change: distributionlist owner" {
        $result = change-distributionlistowner -WebserviceConnection $global:ws -mailaddress $distributionlist -newowner $existingfunctionalMailbox
        $($result.ReturnCode) | should -Be 0
    }

    It "change: globaladdressbook visibility" {
        $result = change-distributionlistglobaladressbookvisibility -WebserviceConnection $global:ws -mailaddress $distributionlist
        $result | should -Be $true
    }

    It "change: distributionlist displayname" {
        $newdisplayname = "Alle Tiere und Zoowaerter"
        $result = change-distributionlistdisplayname -WebserviceConnection $global:ws -mailaddress $distributionlist -newmdisplayname $newdisplayname
        $result | should -Be $true
    }

    It "change: Distributionlist Mailaddress" {
        $result = change-distributionlistemailaddresses -WebserviceConnection $global:ws -mailaddress $distributionlist -newmailaddress $newdistributionmailaddress
        $($result.Returncode) | should -Be 0
    }

    It "add member mailbox to distributionlist" {
        $result = add-distributiongroupmember -WebserviceConnection $global:ws -maillistEMail $distributionlist -newmember $existingfunctionalMailbox
        $result.Returncode | Should -Be 0
    }

    It "add member maillist to distributionlist" {
        $result = add-distributiongroupmember -WebserviceConnection $global:ws -maillistEMail $distributionlist -newmember $seconddistributionlist
        $result.Returncode | Should -Be 0
    }

    It "remove member mailbox from distributionlist" {
        $result = remove-distributiongroupmember -WebserviceConnection $global:ws -maillistEMail $distributionlist -member $existingfunctionalMailbox
        $result.ReturnCode | Should -Be 0
    }

    It "remove member maillist from distributionlist" {
        $result = remove-distributiongroupmember -WebserviceConnection $global:ws -maillistEMail $distributionlist -member $seconddistributionlist
        $result.ReturnCode | Should -Be 0
    }

    It "delete first distribution list" {
        $result = remove-distributionlist -WebserviceConnection $global:ws -mailaddress $distributionlist
        $result | Should -Be $true
    }

    It "delete second distribution list" {
        $result = remove-distributionlist -WebserviceConnection $global:ws -mailaddress $seconddistributionlist
        $result | Should -Be $true
    }
}