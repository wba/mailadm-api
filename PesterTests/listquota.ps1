#functioncall in order to administer quotapools

function get-quotapool {
    [CmdletBinding()]
    param (
        [string]$Domain,
        $WebServiceConnection
    )
    $QuotaPools = $WebServiceConnection.getquotaPoolsForDomain($Domain)        
    $QuotaPools
  
}

function get-quotapoolwithusage {
    [CmdletBinding()]
    param (
        [string]$Domain,
        $WebServiceConnection
    )
    $QuotaPoolsWithUsage = $WebServiceConnection.getquotaPoolsWithUsageForDomain($Domain)        
    $QuotaPoolsWithUsage  
}