# functioncalls in order to administer dristributionlists 

function create-distributionlist {
    [CmdletBinding()]
    param ($WebserviceConnection, $displayname, $mailaddress)
    
    $result = $webserviceconnection.CreateMaillist($displayname, $mailaddress)
    $result
}

function get-distributionlist {
    [CmdletBinding()]
    param ($WebserviceConnection, $mailaddress)

    $distributionlists = $WebserviceConnection.getMailLists()
    $distributionlist = $distributionlists | Where-Object { $_.emailaddress -match $mailaddress }   
    
    $distributionlist
}

function remove-distributionlist {
    [CmdletBinding()]
    param ($WebserviceConnection, $mailaddress)

    $distributionlists = $WebserviceConnection.getMailLists()
    $groupSamAccountName = $distributionlists | Where-Object { $_.emailaddress -match $mailaddress }   

    $result = $WebserviceConnection.RemoveMailList($groupSamAccountName.SamAccountName)
    $result
}

function change-distributionlistowner {
    [CmdletBinding()]
    param ($WebserviceConnection, $mailaddress, $newowner)

    $secondaryowner = ""
    $result = $WebserviceConnection.SetMaillistOwner($mailaddress, $newowner, $secondaryowner)
    $result
}

function change-distributionlistglobaladressbookvisibility {
    [CmdletBinding()]
    param ($WebserviceConnection, $mailaddress)

    $distributionlist = get-distributionlist -WebserviceConnection $WebserviceConnection -mailaddress $mailaddress

    if ($distributionlist.ShowInGlobalAddressbook) {
        $result = $WebserviceConnection.SetMaillistAddToGlobalAddressbook($distributionlist.SamAccountName, $false)
    }
    else {
        $result = $WebserviceConnection.SetMaillistAddToGlobalAddressbook($distributionlist.SamAccountName, $true)
    }
    $result
}

function change-distributionlistemailaddresses {
    [CmdletBinding()]
    param ($WebserviceConnection, $mailaddress, $newmailaddress)

    $distributionlist = get-distributionlist -WebserviceConnection $WebserviceConnection -mailaddress $mailaddress
    $result = $WebserviceConnection.setMaillistEmailaddresses($distributionlist.SamAccountName, $newmailaddress)
    $result
}

function change-distributionlistdisplayname {
    [CmdletBinding()]
    param ($WebserviceConnection, $mailaddress, $newmdisplayname)

    $distributionlist = get-distributionlist -WebserviceConnection $WebserviceConnection -mailaddress $mailaddress
    $result = $WebserviceConnection.setMaillistDisplayname($distributionlist.SamAccountName, $newdisplayname)
    $result
}

function add-distributiongroupmember {
    [CmdletBinding()]
    param ($WebserviceConnection, $maillistEMail, $newmember)

    $global:mailboxes = $webserviceconnection.getMailboxList()
    $global:SamAccountName = $mailboxes | Where-Object {$_.emailaddress -like $newmember} | Select-Object -Property SamAccountName
    if (!($mailboxes | Where-Object {$_.emailaddress -like $newmember})){
        $global:maillists = $WebserviceConnection.getMailLists()
        $global:SamAccountName = $maillists | Where-Object {$_.emailaddress -like $newmember} | Select-Object -Property SamAccountName
    }

    $result = $WebserviceConnection.addMemberToMaillist($maillistEMail, $SamAccountName.SamAccountName)
    $result
}

function remove-distributiongroupmember {
    [CmdletBinding()]
    param ($WebserviceConnection, $maillistEMail, $member)

    $global:mailboxes = $webserviceconnection.getMailboxList()
    $global:SamAccountName = $mailboxes | Where-Object {$_.emailaddress -like $member} | Select-Object -Property SamAccountName
    if (!($mailboxes | Where-Object {$_.emailaddress -like $member})){
        $global:maillists = $WebserviceConnection.getMailLists()
        $global:SamAccountName = $maillists | Where-Object {$_.emailaddress -like $member} | Select-Object -Property SamAccountName
    }

    $result = $WebserviceConnection.RemoveMemberFromMaillist($maillistEMail, $SamAccountName.SamAccountName)
    $result
}