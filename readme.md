# MailAdm API

## Hintergrund

Das IT Center der RWTH Aachen stellt den zentralen Mailserver bereit. Die Verwaltung der Mailadressen wird durch registrierte Ansprechpartner für die jeweiligen Einrichtungen (Maildomänen) über eine Web Application (MailAdm) vorgenommen. Um die Prozesse zur Pflege von Mailadressen in die internen Abläufe automatisch integrieren zu können, wird neben dem MailAdm auch eine entsprechende, auf SOAP basierende, API angeboten, so dass die Pflege der Mailadressen in die vorhandenen lokalen Prozesse der jeweiligen Einrichtung integriert werden kann.

## Voraussetzungen

Zur Nutzung der MailAdm API benötigen Sie einen Serviceaccount. Diesen können Sie anfordern, indem sie eine Mail an servicedesk@itc.rwth-aachen.de schicken. Derzeit ist unsere Schnittstelle mit der Windows Powershell 5.1 getestet.


## Inhalt
**PesterTests** - Dieses Verzeichnis beinhaltet die Wrapper Methoden der eigentlichen API Aufrufe die über ein zentrales Script verknüft sind, das entsprechende Unit Tests beinhaltet
**BeispielAnwendung** - Dieses Verzeichnis beinhalteet eine Demoanwendung, die die Stuerung der Maildomaenenkonfiguration über eine editierbare Excel Datei steuert.


## Autoren

* **Wilfred Gasper**
* **Michael Wirtz**
* **Ketli Cellonari**
* **Julia Opgen-Rhein**
* **Kevin Gostomski**
* **Thomas Pätzold**
* **Jannis Hahn**
* **Mia Möbes**
